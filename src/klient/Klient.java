package klient;

public class Klient {
	private String imie;
	private String nazwisko;
	private String plec;
	private String telefon;
	
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getPlec() {
		return plec;
	}
	public void setPlec(String plec) {
		this.plec = plec;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	//equals a nie == bo == nie zawsze zwroci prawde gdy obiekty sa sobie rowne
	public boolean testKlienta(String imie, String nazwisko){
		return this.imie.equals(imie) && this.nazwisko.equals(nazwisko);
	}
}
