package main;

import java.util.*;
import java.lang.*;

import klient.Klient;
import pojazd.Maluch;
import pojazd.Pojazd;
import pojazd.Suzuki;
import klient.NotFoundClientException;

public class Warsztat {
	private Map<Pojazd, Klient> pojazdy;
	private List<Klient> klienci = new ArrayList<>();
	
	public void main() throws NotFoundClientException{
		int przycisk;
		pojazdy = new TreeMap<Pojazd, Klient>(new Comparator<Pojazd>(){
			@Override
			public int compare(Pojazd p1, Pojazd p2){
				Object o1 = p1;
				Object o2 = p2;
				return o1.getClass().getName().compareTo(o2.getClass().getName());
			}});
		
		//obsluga strumienia wejsciowego(patrzy czy ktos cos wpisal w konsole)
		Scanner scInt = new Scanner(System.in);
		Scanner scLine = new Scanner(System.in);
		
		do{
			System.out.println("Wcisnij 1 aby doda� klienta do bazy");
			System.out.println("Wcisnij 2 aby doda� samochod do bazy");
			System.out.println("Wcisnij 3 aby wyswietlic wszystkie samochody z ich klientami");
			System.out.println("Wcisnij 4 aby wyszukac klienta");
			System.out.println("Wcisnij 9 aby wyjsc z programu");
			
			przycisk = scInt.nextInt();
			
			switch(przycisk){
			case 1 : {
				//dodawanie klienta do bazy
				String imie;
				String nazwisko;
				String plec;
				String telefon;
				System.out.println("Podaj imie: ");
				imie = scLine.nextLine();
				System.out.println("Podaj nazwisko: ");
				nazwisko = scLine.nextLine();
				System.out.println("Podaj plec: ");
				plec = scLine.nextLine();
				System.out.println("Podaj telefon: ");
				telefon = scLine.nextLine();
				
				try{
					//nadanie wartosci zmiennym w obiekcie
					Klient klient = new Klient();
					klient.setImie(imie);
					klient.setNazwisko(nazwisko);
					klient.setPlec(plec);
					klient.setTelefon(telefon);
					
					klienci.add(klient);
					
					System.out.println("Dodano klienta! "+klient.getImie()+" "+klient.getNazwisko());
				}catch(Exception e){}
				
				break;
			}
			case 2 : {
				//dodawanie samochodu do bazy
				String imie;
				String nazwisko;
				
				System.out.print("Podaj imie: ");
				imie = scLine.nextLine();
				System.out.print("Podaj nazwisko: ");
				nazwisko = scLine.nextLine();
				
				Klient znalezionyKlient = znajdzKlienta(imie, nazwisko);
				
				//wyrzuca blad jesli nie bedzie klienta w bazie
				if(znalezionyKlient == null){
					throw new NotFoundClientException();					
				}
				
				System.out.println("Wcisnij 1 aby doda� malucha");
				System.out.println("Wcisnij 2 aby doda� suzuki");
				
				int rodzajPojazdu;
				rodzajPojazdu = scInt.nextInt();
				
				int miejsca;
				int kola;
				double spalanie;
				String rodzajSilnika;
				
				System.out.print("Podaj ilosc miejsc: ");
				miejsca = scInt.nextInt();
				System.out.print("Podaj ilosc kol: ");
				kola = scInt.nextInt();
				System.out.print("Podaj spalanie(l/100km): ");
				spalanie = scInt.nextFloat();
				System.out.print("Podaj rodzaj silnika: ");
				rodzajSilnika = scLine.nextLine();
				
				Pojazd pojazd = null;
				
				switch(rodzajPojazdu){
				case 1:
					int drzwi;
					String testBezpieczenstwa;
					
					System.out.print("Podaj ilosc drzwi: ");
					drzwi = scInt.nextInt();
					System.out.print("Podaj kategorie auta w tescie bezpieczestwa(A, B, C, D): ");
					testBezpieczenstwa = scLine.nextLine();
					
					pojazd = new Maluch(drzwi, testBezpieczenstwa, miejsca, kola, spalanie, rodzajSilnika);
					
					break;
				case 2:
					String rodzaj;
					
					System.out.print("Podaj rodzaj motocyklu: ");
					rodzaj = scLine.nextLine();
					
					pojazd = new Suzuki(rodzaj, miejsca, kola, spalanie, rodzajSilnika);
					
					break;
				}
				
				pojazdy.put(pojazd, znalezionyKlient);
				
				break;
			}
			case 3 :
				//wyswietlanie wszystkich klientow z ich samochodami
				//wyswietlanie elementow mapy z wykorzystaniem iteracji(aby przejrzec elementy mapy)
				for(Map.Entry<Pojazd, Klient> entry : pojazdy.entrySet()){
					System.out.println("Ilosc kol: " + entry.getKey().getKola());
					System.out.println("Rodzaj silnika: " + entry.getKey().getRodzajSilnika());
					System.out.println("Spalanie: " + entry.getKey().getSpalanie());
					System.out.println("Imie: " + entry.getValue().getImie());
					System.out.println("Nazwisko: " + entry.getValue().getNazwisko());
					System.out.println("Telefon: " + entry.getValue().getTelefon());
					System.out.println();
				}
				break;
			case 4 :
				//wyszukiwanie klientow
				String imie;
				String nazwisko;
				
				System.out.print("Podaj imie: ");
				imie = scLine.nextLine();
				System.out.print("Podaj nazwisko: ");
				nazwisko = scLine.nextLine();
				
				Klient znalezionyKlient = znajdzKlienta(imie, nazwisko);
				
				//wyrzuca blad jesli nie bedzie klienta w bazie
				if(znalezionyKlient == null){
					throw new NotFoundClientException();					
				}
				
				System.out.println(znalezionyKlient.getImie() + " " + znalezionyKlient.getNazwisko() + " , " + znalezionyKlient.getPlec() + " , telefon: " + znalezionyKlient.getTelefon() + " ");
				
				break;
			case 9 :
				//wyjscie z programu
				break;
			}
		}while(przycisk != 9);

		
		
	}
	
	//sprawdzamy czy dany klient istnieje w bazie danych
	private Klient znajdzKlienta(String imie, String nazwisko){
		for(Klient klient : klienci){
			if(klient.testKlienta(imie, nazwisko)){
				return klient;
			}
		}
		return null;
	}
	
}
