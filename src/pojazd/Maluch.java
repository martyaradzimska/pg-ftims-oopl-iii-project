package pojazd;

import klient.Klient;

public class Maluch extends Samochod{
	private int drzwi;
	
	public Maluch(int drzwi, String testBezpieczenstwa, int miejsca, int kola, double spalanie, String rodzajSilnika) {
		super(testBezpieczenstwa, miejsca, kola, spalanie, rodzajSilnika);
		this.drzwi = drzwi;
	}
}
