package pojazd;

import java.util.ArrayList;
import java.util.List;

import dokumentacja.Dokumentacja;
import klient.Klient;

public abstract class Pojazd {
    private int kola;
    private double spalanie;
    private String rodzajSilnika;
    private List<Dokumentacja> historia = new ArrayList<>();
    
    public int getKola() {
		return kola;
	}

	public void setKola(int kola) {
		this.kola = kola;
	}

	public double getSpalanie() {
		return spalanie;
	}

	public void setSpalanie(double spalanie) {
		this.spalanie = spalanie;
	}

	public String getRodzajSilnika() {
		return rodzajSilnika;
	}

	public void setRodzajSilnika(String rodzajSilnika) {
		this.rodzajSilnika = rodzajSilnika;
	}

	public Pojazd(int kola, double spalanie, String rodzajSilnika){
    	this.kola = kola;
    	this.spalanie = spalanie;
    	this.rodzajSilnika = rodzajSilnika;
    }
    
    public void dodajDokumentacje(Dokumentacja dokumentacja){
    	historia.add(dokumentacja);
    }
}
