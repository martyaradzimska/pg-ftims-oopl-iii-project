package pojazd;

import klient.Klient;

public abstract class Samochod extends Pojazd {
	private String testBezpieczenstwa;
	private int miejsca;
	
	public Samochod(String testBezpieczenstwa, int miejsca, int kola, double spalanie, String rodzajSilnika) {
		super(kola, spalanie, rodzajSilnika);
		this.testBezpieczenstwa = testBezpieczenstwa;
		this.miejsca = miejsca;
	}
}
