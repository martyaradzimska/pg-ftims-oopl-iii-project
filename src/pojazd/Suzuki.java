package pojazd;

import klient.Klient;

public class Suzuki extends Motor {
	private String rodzaj;
	
	public Suzuki(String rodzaj, int miejsca, int kola, double spalanie, String rodzajSilnika) {
		super(miejsca, kola, spalanie, rodzajSilnika);
		this.rodzaj = rodzaj;
	}
}
